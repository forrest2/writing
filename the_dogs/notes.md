# Notes

Opens with the intro to "The Best Murder Stories" - hosted by Becky and Virginia, a true crime podcast, and Francis Jr listening with his sister Jane. They are now in their 30s. 

Francis stops the podcast when Francis Sr walks in, who asks if the kids want to learn about the man from next door. The kids wonder how/why now, after all this time. Francis Sr had been investigating their former neighbor since that fateful day (the murder). Jane thinks this is a good idea, and the story unfolds with narration by Becky and Virginia.

Flashback to the mid/late 70s, Nico's garden apartment. Nico is elderly and 'eccentric', but seems benign and harmless to the point of Francis Sr and Matthew's children being allowed to go over and bring meals and so forth to him. Nico tends to relate stories of being 'in the war' (ostensibly vietnam) and will often (seemingly unintentionally) say bigoted things. 

an early scene: interior of the family's home (livingroom/dining room/kitchen?) where Francis Sr and Matthew hand Francis Jr a tupperware of food - children's scream is heard from offstage as the body is discovered


Nico is a war vet, and deals with scizophrenia, discharged with 'combat fatigue'
Married before deployment, divorced from wife when scizophrenia symptoms became unbearable
Three (estranged) kids, wife and later children supported him financially without his awareness/involvement
Pattern of paranoia, delusions of self, extreme distrust, sometimes violence
Assaulted a store clerk and a customer back in the early 70s
Was hospitalized and got help, but had a hard time with staying on his meds
Experimented with drugs after returning home, some correlation with onset of illness
War buddies checked in on him and helped him get work and so forth
