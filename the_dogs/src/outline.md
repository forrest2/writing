# The Dogs

## Outline

### Story

> A severely mentally ill man, Nico, becomes confused and agitated due to a hallucination and becomes agressive towards a home health aide. An altercation takes place in his apartment, and ultimately Nico kills the aide with a small radio. Nearly catatonic, Nico 'passes out' in his recliner.

> Unaware of this act of violence or of Nico's illness, the family next door sends their daughter with a warm plate of dinner over to Nico's apartment in an attempt to show neighborly love. When he doesn't answer, Jane lets herself into the apartment and discovers Nico humming to the music on the radio, placed back on the table, covered in blood. As Jane's fathers hear her screaming, Matthew calls 911 and Francis Sr bolts out their door towards Nico's apartment. Francis Jr clings to his father's leg as they await the police and ambulance.

> In the immediate aftermath, both police and local media have come to investigate. Matthew answers the reporter's questions on camera while Jane and her father sit together and speak with the detective.

> A few months have passed, and Jane and Francis Jr both are in a session with Dr William. The children explain their relationship to Nico, describing him as "nice" and "silly, making up tall tales" as they paint an almost charming picture of the man. Francis Sr waits in the waiting room, becoming upset that the TV in the waiting area was tuned to a true crime show. His reading material is a marbled notebook.