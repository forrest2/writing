# "They look homeless"

The statement "The crowd is starting to diversify (as we get closer to [the event's start], fewer people look homeless) which I take as a positive sign" is a subjective statement as it is not based on hard facts and is weighed upon by personal opinion. The idea that this statement is somehow *not* a value judgement is somewhat difficult to pick apart, so it's important to define what a value judgement is.

> Value Judgement: Subjective assessment that a behavior, object, person, principle, etc., is good or bad or something is ought to or not ought to happen. Value judgments involve aesthetical, ethical, ideological, moral, theological and other interpretations and cannot be reduced to arguable statements of fact.

Why might the statement above look like a value judgement? In the sentence itself, the term 'homeless' detracts from the subject (in this case, the attendees of the event) and implies that appearing in a particular way reduces the mix of patrons at the event. In this sense, coupled with the later part of the statement in which diversification adds positivity, the term 'homeless' contextually adds a negative value to the audience of the event. Furthering the idea that this statement is a value judgement, this statement was based on a subjective analysis of the forming crowd, and not an objective view. 

When a subject is described as appearing in a specific way, socially their described appearance adds additional positive or negative value. 'Dressing the part' often helps job-seekers in successfully getting job offers, and the reverse is also true - if a person appears to be disheveled, they likely will also be weighed as being inappropriate, ill-prepared, or unprofessional. Likewise, the appearance of someone being homeless automatically has a social idea around this concept. An informal survey conducted over the last two days includes the following terms that are associated with the concept of homeless appearance:

|Looking homeless|            |
|----------------|------------|
|Disheveled      |Unkempt     |
|Dirty           |Threadbare  |
|Wrinkled        |Unclean     |
|Baggy           |Sick        |
|Poor            |Mentally ill|
|Street dwelling |Addict      |

All of these terms (above) generally are seen as pejorative, and the average person would assign a negative value to these descriptors, subsequently adding a negative value to the person being described. The reality of poverty is much more complex. The US Department of Education and the US Department of Housing and Urban Development have conflicting ideas of what constitutes homelessness, but by and large, homelessness can be defined as the following:

> Homelessness is the state of not having a permanent residence, encompassing persons dwelling in public areas, residing in vehicles, transient living such as the use of emergency or transitional shelter, and the use of temporary housing including 'couch surfing'.

A person equating another as appearing to be homeless often will not think about how this other person would feel if they understood they were being described in this manner, let alone how it might make others within their social circle feel. The intention might be neutral, or even noble, but using the term homeless to describe another can do incredible harm to multiple parties; homeless persons themselves, persons that have been without stable housing in the past, or persons at risk of becoming homeless. 

What kind of harm? This kind of speech reinforces the idea that homeless persons are not to be associated with, and reinforces that only a specific _kind of person_ can and should be considered as being homeless. In fact, persons that are seeking government assistance that do not appear to be impoverished statistically have a more difficult time getting assistance compared to when they are 'dressed the part'. Because it's not always visible what a person's life experience is, words can do harm, and in the instance of making statements meant to be innocent intent doesn't really matter.  

In progressive social circles, homelessness (and the aforementioned terms associated with poverty and unstable housing) is not viewed in a _de facto_ negative light, and often, individuals within these circles will participate in outreach programs in order to help those in poverty, and work to take preventative measures in helping those in their social group to not become homeless. Persons in these circles will also do what they're able to to reduce the pejorative associated with the state of being homeless. Developing empathy and working to understand what homelessness truly 'looks like' is therefore key. 
